#ifndef SIMPLE_SERVER_HPP
#define SIMPLE_SERVER_HPP

#include "DataQueue.hpp"
#include <map>
#include <mutex>
#include <string>
#include <vector>

/*
 * Windows: Overlapped I/O
 * Linux: epoll
 */

#if defined(_WIN32) || defined(_WIN64)
	#include <winsock2.h>
	#include <windows.h>
	#include <ws2tcpip.h>
#endif

#define SIMPLE_SERVER_RECV_BUF_SZ 512
#define SIMPLE_SERVER_SEND_BUF_SZ 512

class SimpleServer
{
	public:
		SimpleServer(DataQueue &q, int maxConnections);
		virtual ~SimpleServer();

		void bind(int port);
		void accepterThread();
    void process();

		int getCurrentConnections() const;
		void setMaxConnections(int maxConnections);

	private:
    void addEvent(HANDLE event);

		SOCKET bindSock;

		DataQueue &queue;
		int maxConnections;

		struct SocketInfo
		{
      SocketInfo() {
        this->recvBuffer.buf = privRecvBuffer;
        this->recvBuffer.len = SIMPLE_SERVER_RECV_BUF_SZ;
        this->sendBuffer.buf = privSendBuffer;
        this->sendBuffer.len = SIMPLE_SERVER_SEND_BUF_SZ;
        this->recvFlags = 0;
        this->sendBufferSize = 0;
        this->recvBufferSize = 0;
      }

			int socket;
      struct sockaddr_in clientInfo;
			bool socketActive; // ? TODO ? check if socket connected
			HANDLE eventRecv;
      OVERLAPPED overlap;
      HANDLE eventSend;
			OVERLAPPED overlapSend;
      WSABUF recvBuffer;
      char privRecvBuffer[SIMPLE_SERVER_RECV_BUF_SZ];
      int recvBufferSize;
      char privSendBuffer[SIMPLE_SERVER_RECV_BUF_SZ];
      int sendBufferDataSent;
      int sendBufferSize;
      WSABUF sendBuffer;
      int sendFlags;
      int recvFlags;
		};

    mutable std::mutex socketsMutex;
		std::map< HANDLE /*event*/, SocketInfo > sockets;

    mutable std::mutex eventsMutex;
    std::vector<HANDLE> eventsToCheck;
};

#endif
