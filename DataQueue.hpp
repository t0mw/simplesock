#ifndef DATA_QUEUE_HPP
#define DATA_QUEUE_HPP

#include <list>
#include <mutex>

enum DataReadWrite
{
	DATA_READ_FROM_CLIENT,
	DATA_WRITE_TO_CLIENT,
};

class DataQueue
{
	public:
		struct Data
		{
			int socket;
			std::string ip;
			int port;
			enum DataReadWrite r_w;
			std::string value;
		};

		DataQueue();
		DataQueue(const DataQueue &cp);
		virtual ~DataQueue();

		// Push & Pop are guarded by mutex.
		void push(struct Data d);
		DataQueue::Data pop();
		bool empty() const;

		int getSize() const;

	private:
		mutable std::mutex vMutex;
		std::list< DataQueue::Data > v;
};

#endif
