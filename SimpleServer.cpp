#include "DataQueue.hpp"
#include "SimpleServer.hpp"
#include <thread>
#include <iostream>

SimpleServer::SimpleServer(DataQueue &q, int maxConnections)
: queue(q)
{
	this->maxConnections = maxConnections;
}

SimpleServer::~SimpleServer()
{
	::closesocket(this->bindSock);
	WSACleanup();
}

void SimpleServer::bind(int port)
{
	WSADATA wsaData;

	int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != NO_ERROR)
		throw std::runtime_error("WSAStartup failed");

	this->bindSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->bindSock == INVALID_SOCKET)
	{
		WSACleanup();
		throw std::runtime_error("Error creating socket");
	}

	sockaddr_in service;
	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr("0.0.0.0");
	service.sin_port = htons(port);

	int res = ::bind(this->bindSock, (SOCKADDR *)&service, sizeof(service));
	if(res == SOCKET_ERROR)
	{
		WSACleanup();
		::closesocket(this->bindSock);
		throw std::runtime_error("Bind failed");
	}

	if( listen(this->bindSock, 1) == SOCKET_ERROR )
	{
		WSACleanup();
		::closesocket(this->bindSock);
		throw std::runtime_error("Listen failed");
	}
}

void SimpleServer::accepterThread()
{
  std::cout << "accepterThread ready\n" << std::endl;

  while(true) 
  {
    SOCKET acceptSock;
	  struct sockaddr_in clientInfo;
	  int addrSize = sizeof(clientInfo);

	  // Accept any new connection.
	  acceptSock = accept(this->bindSock, (sockaddr *)&clientInfo, &addrSize);
	  if(acceptSock != INVALID_SOCKET)
	  {
		  SocketInfo si;
		  si.socket = acceptSock;
      si.clientInfo = clientInfo;
		  si.eventRecv = WSACreateEvent();

      std::string ip(inet_ntoa(clientInfo.sin_addr));
      std::cout << "Accepted new connection: "
                << ip
                << ":"
                << ntohs(clientInfo.sin_port)
                << std::endl;

		  if( si.eventRecv == NULL )
		  {
			  throw std::runtime_error("Could not create event");
		  }

		  si.overlap.hEvent = si.eventRecv;

      // Run overlapped I/O recv for the new socket.
      // http://tinyclouds.org/iocp-links.html
      // I/O completion ports - https://msdn.microsoft.com/pl-pl/library/windows/desktop/aa365198(v=vs.85).aspx
      // http://www.winsocketdotnetworkprogramming.com/winsock2programming/winsock2advancedscalableapp6b.html
      // https://en.wikipedia.org/wiki/Overlapped_I/O
      // https://msdn.microsoft.com/en-us/library/windows/desktop/ms741582(v=vs.85).aspx
      // https://msdn.microsoft.com/en-us/library/windows/desktop/ms741688(v=vs.85).aspx
      // https://msdn.microsoft.com/en-us/library/windows/desktop/ms742203(v=vs.85).aspx
      // https://msdn.microsoft.com/en-us/library/windows/desktop/aa365603(v=vs.85).aspx

      int res = WSARecv(si.socket,
        &si.recvBuffer,
        1,
        (LPDWORD)&si.recvBufferSize,
        (LPDWORD)&si.recvFlags,
        &si.overlap,
        NULL);

      if ((res == SOCKET_ERROR) && (WSA_IO_PENDING != WSAGetLastError())) {
        throw std::runtime_error("WSARecv failed");
      }

      std::lock_guard<std::mutex> l(this->socketsMutex);
      this->sockets.insert(std::make_pair(si.overlap.hEvent, si));

      // Not protecting with mutex intentionally.
      this->eventsToCheck.push_back(si.overlap.hEvent);
    }
  }
}

void SimpleServer::process()
{
  // Run accepter thread.
  std::thread thr(&SimpleServer::accepterThread, this);

  int eventsSize = 0;

  while(true)
  {
    eventsSize = this->eventsToCheck.size();
    if(eventsSize == 0)
    {
      Sleep(200);
      continue;
    }

    DWORD ret = WSAWaitForMultipleEvents(eventsSize,
      &eventsToCheck[0],
      FALSE,
      INFINITE,
      TRUE);

    if (ret == WSA_WAIT_FAILED) {
      throw std::runtime_error("WSAWaitForMultipleEvents failed with error");
    }

    std::cout << "WSAWaitForMultipleEvents returned: " << ret << std::endl;

    if( ret >= WSA_WAIT_EVENT_0 && ret < ( WSA_WAIT_EVENT_0 + eventsSize) )
    {
      // Find what's the socket data for this event.
      SocketInfo &si = sockets.at(eventsToCheck[ret]);

      BOOL rc = FALSE;

      rc = WSAGetOverlappedResult(si.socket,
                                  &si.overlap,
                                  (LPDWORD)&si.recvBufferSize,
                                  FALSE, // Do not wait.
                                  (LPDWORD)&si.recvFlags);

      if (rc == FALSE)
        std::cout << "WSAGetOverlappedResult READ returned FALSE" << std::endl;

      if (si.recvBufferSize != 0)
      {
        std::cout << "Socket: " << si.socket << std::endl
          << "IP: " << std::string(inet_ntoa(si.clientInfo.sin_addr))
          << std::endl;
        std::cout << "Read " << si.recvBufferSize << " bytes." << std::endl;
        for (int i = 0; i < si.recvBufferSize; ++i)
          std::cout << si.recvBuffer.buf[i];
        std::cout << std::endl << std::endl;

        // Insert the read data to DataQueue
        //DataQueue::Data d;
        //d.socket = 
        //d.ip = 
        //d.port =
        //d.r_w = 
        //d.value =
        //queue.push(d);
        // Prepare new read event.
      }
      else
      {
        std::lock_guard<std::mutex> l1(socketsMutex);
        std::lock_guard<std::mutex> l2(eventsMutex);
        sockets.erase(eventsToCheck[ret]);
        eventsToCheck.erase(eventsToCheck.begin() + ret);
        continue;
      }

      WSAResetEvent(si.overlap.hEvent);
      int res = WSARecv(si.socket,
        &si.recvBuffer,
        1,
        (LPDWORD)&si.recvBufferSize,
        (LPDWORD)&si.recvFlags,
        &si.overlap,
        NULL);

      if ((res == SOCKET_ERROR) && (WSA_IO_PENDING != WSAGetLastError()))
        throw std::runtime_error("WSARecv failed");

      // Move the current firing event to the end of the queue.
      eventsMutex.lock();
        HANDLE h = *(eventsToCheck.begin());
        eventsToCheck.erase(eventsToCheck.begin());
        eventsToCheck.push_back(h);
      eventsMutex.unlock();
    }
  }
}

int SimpleServer::getCurrentConnections() const
{
	return this->sockets.size();
}

void SimpleServer::setMaxConnections(int maxConnections)
{
	this->maxConnections = maxConnections;
}
