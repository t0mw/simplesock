#ifndef SIMPLE_CLIENT_HPP
#define SIMPLE_CLIENT_HPP

#include <string>
#include <vector>

#if defined(_WIN32) || defined(_WIN64)
	#include <winsock2.h>
	#include <windows.h>
	#include <ws2tcpip.h>
#endif

class SimpleClient
{
	public:
		SimpleClient();
		virtual ~SimpleClient();

		void connect(const std::string &host, int port);
		void disconnect();
		void write(const std::string &data);
		std::vector<char> read(int max = 0, int timeout_s = 2);

	private:
		std::string getIpByHost(const std::string &host, int port);

		#if defined(_WIN32) || defined(_WIN64)
			SOCKET sock;
		#endif
};

#endif
