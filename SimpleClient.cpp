#include "SimpleClient.hpp"
#include <string>
#include <stdexcept>
#include <sstream>
#include <vector>
#include <iostream>

SimpleClient::SimpleClient()
{

}

SimpleClient::~SimpleClient()
{
	::closesocket(this->sock);
	WSACleanup();
}

void SimpleClient::connect(const std::string &host, int port)
{
	WSADATA wsaData;

	int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != NO_ERROR)
		throw std::runtime_error("WSAStartup failed");

	this->sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->sock == INVALID_SOCKET)
	{
		WSACleanup();
		throw std::runtime_error("Error creating socket");
	}

	sockaddr_in service;
	memset(&service, 0, sizeof(service));
	service.sin_family = AF_INET;

	std::string ip = this->getIpByHost(host, port);

	service.sin_addr.s_addr = inet_addr(ip.c_str());
	service.sin_port = htons(port);

	if (::connect(this->sock, (SOCKADDR *)&service, sizeof(service)) == SOCKET_ERROR)
	{
		WSACleanup();
		throw std::runtime_error("Failed to connect");
	}
}

void SimpleClient::disconnect()
{
	::closesocket(this->sock);
	WSACleanup();
	this->sock = 0;
}

void SimpleClient::write(const std::string &data)
{
	int bytesToSend = data.length();
	const char *d = data.c_str();
	int bytesSent = 0;

	do 
	{
		const char *ptr = d + bytesSent;
		int alreadySend = send(this->sock, ptr, bytesToSend, 0);
		std::cout << "Bytes send: " << alreadySend << std::endl;
		bytesToSend -= alreadySend;
		bytesSent += alreadySend;
	}
	while (bytesToSend != 0);
}

std::vector<char> SimpleClient::read(int max /*= 0*/, int timeout_s /*= 2*/)
{
	std::vector<char> receivedData;

	int timeout = timeout_s * 1000;
	setsockopt(this->sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));

	char buf[1024];
	int bytesRecv = 0;
	bool stopFlag = false;
	do
	{
		bytesRecv = recv(this->sock, buf, 1024, 0 );
		std::cout << "Received:" << bytesRecv << std::endl;

		if(bytesRecv < 0)
		{
			if( bytesRecv == SOCKET_ERROR && WSAGetLastError() == WSAEWOULDBLOCK)
				continue;

			if( bytesRecv == SOCKET_ERROR && WSAGetLastError() == WSAETIMEDOUT )
				break;

			throw std::runtime_error("Socket recv error");
		}

		if (bytesRecv == 0)
			break;

		for(int i = 0; i < bytesRecv; ++i )
			receivedData.push_back(buf[i]);
	}
	while(!stopFlag);

	return receivedData;
}

std::string SimpleClient::getIpByHost(const std::string &host, int port)
{
	std::stringstream ss;
	ss << port;

	std::string portStr(ss.str());

	struct addrinfo *result = NULL;
	struct addrinfo hints;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	int ret = getaddrinfo(host.c_str(), portStr.c_str(), &hints, &result);

	if( ret != 0 )
	{
		WSACleanup();
		throw std::runtime_error("getaddrinfo failed");
	}

	// Assume IPv4 address, fix later.
	struct sockaddr_in *sockaddr_ipv4 = (struct sockaddr_in *)result->ai_addr;

	return std::string(inet_ntoa(sockaddr_ipv4->sin_addr));
}
