#include "DataQueue.hpp"
#include <mutex>
#include <iostream>

DataQueue::DataQueue()
{

}

DataQueue::DataQueue(const DataQueue &cp)
{
	std::lock_guard<std::mutex> l(this->vMutex);
	this->v = cp.v;
}

DataQueue::~DataQueue()
{

}

void DataQueue::push(struct Data d)
{
	std::lock_guard<std::mutex> l(this->vMutex);
	this->v.push_back(d);
}

DataQueue::Data DataQueue::pop()
{
	std::lock_guard<std::mutex> l(this->vMutex);
	DataQueue::Data d;
	d = this->v.front();
	this->v.pop_front();
	return d;
}

bool DataQueue::empty() const
{
	return this->v.empty();
}

int DataQueue::getSize() const
{
	std::lock_guard<std::mutex> l(this->vMutex);
	return this->v.size();
}
