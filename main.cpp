#include "SimpleClient.hpp"
#include "SimpleServer.hpp"
#include "DataQueue.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <thread>
#include <memory>

void dataQueueWorker(DataQueue &q)
{
	std::cout << "Queue worker running\n";
	bool stopFlag = false;
	while( !stopFlag )
	{
		if( q.empty() == false )
		{
			for( int i = 0; i < q.getSize(); ++i )
			{
				DataQueue::Data d = q.pop();
				std::cout << "Processing stuff from: "
					  << d.ip << " : "
					  << d.port << "\n"
					  << "R/W: " << ( d.r_w == DATA_READ_FROM_CLIENT ? "READ" : "WRITE" )
					  << " Data: " << d.value << std::endl;

				// If this is write, push back to the queue.
				// This will be handled by SimpleServer::process
				if( d.r_w == DATA_WRITE_TO_CLIENT )
				{
					q.push(d);
					continue;
				}
			}
		}
	}
}

int main()
{
	//SimpleClient c;
	//c.connect("google.pl", 80);
	//// c.connect("127.0.0.1", 10000);
	//c.write("GET / HTTP/1.1\r\n\r\n\r\n");
	////Sleep(3000);
	//std::vector<char> s = c.read();

	//std::string str;
	//std::for_each(	s.begin(), 
	//		s.end(), 
	//		[&](char c) { str += c; } );

	//std::cout << str << std::endl;

	DataQueue q;
	SimpleServer s(q, 10);
	s.bind(8080);

	std::shared_ptr< std::thread * > queueWorker =
		std::make_shared< std::thread * >(
			new std::thread(dataQueueWorker, std::ref(q))
		);

	//bool stopFlag = false;
	//while(!stopFlag)
	//{
		s.process();
	//}

	system("pause");
}
